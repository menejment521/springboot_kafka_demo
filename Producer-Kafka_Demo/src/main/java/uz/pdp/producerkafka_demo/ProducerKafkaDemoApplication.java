package uz.pdp.producerkafka_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProducerKafkaDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProducerKafkaDemoApplication.class, args);
    }

}
