package uz.pdp.consumerkafka_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerKafkaDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerKafkaDemoApplication.class, args);
    }

}
